import React from 'react';
var FontAwesome = require('react-fontawesome');

const MovieList = (props) => {
    
    const movieClicked = movie => event => {
        props.movieClicked(movie);
    };

    const editOnClick = movie => {
        props.editOnClick(movie);
    }

    const removeOnClick = movie => {
        fetch(`${process.env.REACT_APP_URL}/api/movies/${this.props.movie.id}/`, {
            method: 'DELETE',
            headers: {
                'COntent-Type': 'application/json',
                'Authorization': 'Token ce5e2b3c8de79cdbeb06e805397a13a7bc098fe9'
            },
            }).then( response => props.movieDeleted(movie))
            .catch(error => console.log(error))
    };

    const newMovie = () => {
        props.newMovie();
    }
    return (
        <div>
            { props.movies.map(movie => {
                return (
                    <div key={movie.id} className="movie-item">
                        <h3 onClick={movieClicked(movie)}>
                            {movie.title}</h3>
                            <FontAwesome name="edit" onClick={() => editOnClick(movie)} />
                            <FontAwesome name="trash" onClick={() => removeOnClick(movie)} />
                    </div>
                )
            })}
            <button onClick={newMovie} >Add New</button>
        </div>
    );
}

export default MovieList;