import React, { Component } from 'react';
// var FontAwesome = require('react-fontawesome');


class MovieForm extends Component {

    state = {
        editedMovie: this.props.movie
    }

    cancelOnClick = () => {
        this.props.cancelForm();
    }
    inputChanged = (event) => {
        let movie = this.state.editedMovie;
        movie[event.target.name] = event.target.value;
        this.setState({editedMovie: movie});
    }
    saveOnClick = () => {
        console.log(this.state.editedMovie);
        fetch(`${process.env.REACT_APP_URL}/api/movies/`, {
            method: 'POST',
            headers: {
                'COntent-Type': 'application/json',
                'Authorization': 'Token ce5e2b3c8de79cdbeb06e805397a13a7bc098fe9'
            },
            body: JSON.stringify(this.state.editedMovie)
            }).then( response => response.json())
            .then(resp => this.props.newMovie(resp))
            .catch(error => console.log(error))
    }
    updateOnClick = () => {
        console.log(this.state.editedMovie);
        fetch(`${process.env.REACT_APP_URL}/api/movies/${this.props.movie.id}`, {
            method: 'PUT',
            headers: {
                'COntent-Type': 'application/json',
                'Authorization': 'Token ce5e2b3c8de79cdbeb06e805397a13a7bc098fe9'
            },
            body: JSON.stringify(this.state.editedMovie)
            }).then( response => response.json())
            .then(resp => this.props.editedMovie(resp))
            .catch(error => console.log(error))
    }
   
    render () {

        const isDisabled = this.state.editedMovie.title.length === 0 ||
                            this.state.editedMovie.description.length === 0;

        return (
            <React.Fragment>
               <span>Title</span><br/>
               <input type="text" name="title" value={this.props.movie.title} onChange={this.inputChanged}/><br/>
               <span>Description</span><br/>
               <textarea name="description" value={this.props.movie.description} onChange={this.inputChanged} /><br/>
               { this.props.movie.id ? <button disabled={isDisabled} onClick={this.updateOnClick}>Upate</button> : <button disabled={isDisabled} onClick={this.saveOnClick}>Save</button>}
               &nbsp;
               <button onClick={this.cancelOnClick}>Cancel</button>
            </React.Fragment>
        )
    }
}


export default MovieForm;