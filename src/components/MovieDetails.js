import React, { Component } from 'react';
var FontAwesome = require('react-fontawesome');


class MovieDetails extends Component {

    state = {
        highlighted: -1
    }

    highlightRate = highlight => event => {
        this.setState({highlighted: highlight});
    }

    rateOnClick = stars => event => {
        fetch(`${process.env.REACT_APP_URL}/api/movies/${this.props.movie.id}/rate_movie/`, {
            method: 'POST',
            headers: {
                'COntent-Type': 'application/json',
                'Authorization': 'Token ce5e2b3c8de79cdbeb06e805397a13a7bc098fe9'
            },
            body: JSON.stringify({stars: stars + 1})
            }).then( response => response.json())
            .then(resp => this.getDetails())
            .catch(error => console.log(error))
    }

    getDetails = () => {
        fetch(`${process.env.REACT_APP_URL}/api/movies/${this.props.movie.id}/`, {
            method: 'GET',
            headers: {
                'COntent-Type': 'application/json',
                'Authorization': 'Token ce5e2b3c8de79cdbeb06e805397a13a7bc098fe9'
            },
            }).then( response => response.json())
            .then(resp => this.props.updateMovie(resp))
            .catch(error => console.log(error))
    }

    render (){
        const mov = this.props.movie;

        return (
            <React.Fragment>
                { mov ? (
                    <div>
                        <h3>{mov.title}</h3>
                        <FontAwesome name="star" className={mov.avg_rating > 0 ? 'orange' : ''} />
                        <FontAwesome name="star" className={mov.avg_rating > 1 ? 'orange' : ''} />
                        <FontAwesome name="star" className={mov.avg_rating > 2 ? 'orange' : ''} />
                        <FontAwesome name="star" className={mov.avg_rating > 3 ? 'orange' : ''} />
                        <FontAwesome name="star" className={mov.avg_rating > 4 ? 'orange' : ''} />
                        ({mov.no_of_ratings})
                        <p>{mov.description}</p>

                        <div className="rate-container">
                            <h2>Rate it!!!</h2>
                            { [...Array(5)].map( (element, index) => {
                                return <FontAwesome key={index} name="star" className={this.state.highlighted > index - 1 ? 'purple' : ''} 
                                onMouseEnter={this.highlightRate(index)} onMouseLeave={this.highlightRate(-1)} onClick={this.rateOnClick(index)} />
                            })}
                        </div>
                    </div>
                ) : null }
            </React.Fragment>
        )
    }
}


export default MovieDetails;