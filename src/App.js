import React, {Component} from 'react';
import './App.css';
import MovieList from './components/MovieList';
import MovieDetails from './components/MovieDetails';
import MovieForm from './components/MovieForm';
var FontAwesome = require('react-fontawesome');



class App extends Component {

    state = {
        movies: [],
        selectedMovie: null,
        editedMovie: null
    }
    

    componentDidMount(){
        fetch('http://127.0.0.1:8000/api/movies/', {
            method: 'GET',
            headers: {
                'Authorization': 'Token ce5e2b3c8de79cdbeb06e805397a13a7bc098fe9'
            }
        }).then( response => response.json())
        .then(resp => this.setState({movies: resp}))
        .catch(error => console.log(error))
    }


    loadMovie = movie => {
        this.setState({selectedMovie: movie, editedMovie: null});
    }

    movieDeleted = selMovie => {
        const movies = this.state.movies.filter(movie => movie.id !== selMovie.id);
        this.setState({movies: movies, selectedMovie: null})
    }
    
    editOnClick = selMovie => {
        this.setState({editedMovie: selMovie});
    }

    newMovie = () => {
        this.setState({editedMovie: {title: '', description: ''}});
    }

    cancelForm = () => {
        this.setState({editedMovie: null});
      }
    addMovie = movie => {
        this.setState({movies: [...this.state.movies, movie]});
      }


    render() {
        return (
            <div className="App">
                <h1>
                    <FontAwesome name="film" />
                    <span>Rate Movies Here</span>
                </h1>
                <div className="layout">
                    <MovieList 
                      movies={this.state.movies} 
                      movieClicked={this.loadMovie} 
                      editOnClick={this.editOnClick}
                      movieDeleted={this.movieDeleted}
                      newMovie={this.newMovie} /> 
                    <div>
                        { !this.state.editedMovie ?
                            <MovieDetails movie={this.state.selectedMovie} updateMovie={this.loadMovie} />
                        : <MovieForm movie={this.state.editedMovie} cancelForm={this.cancelForm} newMovie={this.state.addMovie} editedMovie={this.loadMovie}/>}
                        
                    </div>
                </div>
                
            </div>
        );
    }
    
}


export default App;